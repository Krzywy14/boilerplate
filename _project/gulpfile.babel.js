'use strict'

import gulp			from 'gulp';
import config		from './tasks/config';
import browserSync	from 'browser-sync';
import runSequence 	from 'run-sequence';

// gulp tasks

import styles				from "./tasks/styles";
import clean				from "./tasks/clean";
import move, {selectify} 	from "./tasks/move";
import scripts				from "./tasks/scripts";
import images				from "./tasks/images";
import watch				from "./tasks/watch";
import sql					from "./tasks/sql";


gulp.task('styles',			styles);
gulp.task('move',			move);
gulp.task('move:selected',  selectify);
gulp.task('scripts',		scripts);
gulp.task('images',			images);
gulp.task('clean',			clean);
gulp.task('watch',			watch);
gulp.task('sql',			sql);


gulp.task('run', (done) =>{
	return runSequence('build',['watch','move:selected'], done);
});

gulp.task('default', ['styles','scripts','move','images','watch']);

gulp.task('build',['scripts','styles','images','move']);