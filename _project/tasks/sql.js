'use strict'

import gulp			from "gulp";
import mysqlDump	from "mysqldump";
import reportErr	from './notify';

import config		from './config';

export default () => {
	mysqlDump({
		host: config.sql.host,
		user: config.sql.user,
		password: config.sql.password,
		database: config.sql.database,
		dest:'./data.sql' // destination file 
	},function(err){})
}