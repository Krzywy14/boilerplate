'use strict';

import gulp 		from 'gulp';
import watch		from 'gulp-watch';
import config		from './config'

import browserSync	from 'browser-sync';

export default () => {
	browserSync.create();
	browserSync.init({
		ui: {
		    port: config.BS.ui_port
		},
		port: 		config.BS.port,
        proxy: 		config.BS.proxy,
        logLevel: 	config.BS.logLevel,
        files: [
			{
			match: ["../**/*.php"],
				fn: function (event, file) {
					browserSync.reload();
				},
			}
		]

    });
	gulp.watch( config.styles.source, 		['styles']);
	gulp.watch( config.images.source, 		['images']);
	gulp.watch( config.scripts.allsource, 	['scripts']);
}