<input type="checkbox" id="m_nav">
<div class="header--wrap">
	<div class="logo fixed">
		<a href="<?php echo $home ?>">
			<?php get_img("logo.png") ?>
		</a>
	</div>
	<label for="m_nav" id="mobile-hamburger" onclick>
		<span></span>
		<span></span>
		<span></span>
		<span></span>
	</label>
</div>
<nav>
	<ul>
		<?php foreach ($navs as $nav_item) { ?>
			<li><a href="<?= $nav_item['href'] ?>"><?= $nav_item['name'] ?></a></li>
		<?php } ?>
	</ul>
</nav>